# Hello_NP

Test Java new project created from Intellij.

## 🔧 Build jar

如果要製作 `.jar` 檔案, 最好從 Intellij 裡面建立. 因為使用 `mvn` 建立不起來... 

看 [How to create a jar file with IntelliJ IDEA](https://www.youtube.com/watch?v=3Xo6zSBgdgk&t=23s&ab_channel=ArturSpirin).
